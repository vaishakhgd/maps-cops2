package codingwithmitch.com.googlemapsgoogleplaces;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  {

    private static final String TAG = "MainActivity";

    private static final int ERROR_DIALOG_REQUEST = 9001;

    public static List<ParseObject> results_new;


    public void foo2(TextView tv,String s){

        tv.setText(s);

    }

    public void foo(final TextView tv){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("reminderList");

        query.findInBackground(new FindCallback<ParseObject>() {

                                   public void done(List<ParseObject> results, ParseException e) {
                                       if (e == null) {
                                           results_new=results;
                                           String s1="as";
                                           for(int i=0;i<results_new.size();i++){
                                               double d1=results_new.get(i).getDouble("latitude");
                                               s1+=String.valueOf(d1);


                                           }
                                           foo2(tv,s1);

                                       }
                                   }
                               }
        );
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View layout = (View)findViewById(R.id.view);




        results_new=new ArrayList<ParseObject>();

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("Mr42bvuYLKepnLgLH7tmDbHWOy92SNRkvLqwVsvN")
                // if defined
                .clientKey("GaGOsPemJvW88s0OFHVe0EXDapJ5Jo2DZylD5pj1")
                .server("https://parseapi.back4app.com/")
                .build()
        );


        ParseInstallation.getCurrentInstallation().saveInBackground();

       TextView tv = (TextView) findViewById(R.id.textView);

       foo(tv);




        try {
            String path1="1.txt";
            File file = getFileStreamPath(path1);

            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream writer = openFileOutput(file.getName(), Context.MODE_PRIVATE);

            String string="ninn";
                writer.write(string.getBytes());
                writer.flush();


            writer.close();
        }
        catch(IOException e){e.printStackTrace();}

        if(isServicesOK()){
            init();
        }
    }

    private void init(){
        Button btnMap = (Button) findViewById(R.id.btnMap);
        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
    }

    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MainActivity.this);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MainActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

}






















